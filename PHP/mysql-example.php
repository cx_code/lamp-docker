<h2 style="color:DarkBlue"> Przykład użycia LAMP </h2>

Zawsze można powrócić do poprzedniego ekranu klikając:
<br><a href="index.php">Powrót</a>

<h3 style="color:Blue"> Zasada działania: </h3>
Poniżej znajduje się tabela przedstawiająca wszystko co zawarte jest w bazie danych.
<br>Jest to przykładowy zestaw studentów, który można edytować poprzez:

<ul>
  <li>Tworzenie nowych rekordów,</li>
  <li>Edycję już istniejących,</li>
  <li>Usuwanie istniejących</li>
</ul>

<br>
<br> Poniżej rozpoczyna się działanie właściwego programu:

<h4> Nawiązywanie połączenia z bazą MySQL... </h4>
Poniżej powininny być widoczne logi z działania połączenia:<br>

<table border="1">
    <tr>
        <th>Id</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Email</th>
        <th>Indeks</th>
        <th>Usuń</th>
    </tr>

<?php
$host = 'mysql';
$user = 'root';
$pass = 'password';
$database = 'StudentsDatabase';
$mysqli = new mysqli( $host, $user, $pass, $database );


if( $mysqli->connect_error )
{
    die( "Połączenie nie powiodło się: " . $mysqli->connect_error );
}
else
{
    $table = 'Students';
    if( $result = $mysqli->query( "SHOW TABLES LIKE '".$table."'" ) )
    {
        if( $result->num_rows == 1 )
        {
            $sql = "SELECT id, firstname, lastname, email, studentIndex FROM Students";
            $result = $mysqli->query($sql);

            if( $result->num_rows > 0 )
            {
                echo "<form action=\"mysql-student-delete.php\" method=\"post\">";
                while( $row = $result->fetch_assoc() )
                {
                    echo "<tr><td>" . $row["id"]. "</td><td>" . $row["firstname"] . "</td><td>" . $row["lastname"]. "</td><td>" . $row["email"]. "</td><td>" . $row["studentIndex"]. "</td><td>";
                    echo "<div id=\"delete\"><input type=\"submit\" name=\"id\" value=".$row['id']." /></div></td></tr>";
                }
                echo "</form>";
            }
        }
        else
        {
            include 'table-creating-error.php';
            CreateTableOnError();
        }
    }
}
?>
</table>

<br>
<br>

<form action="mysql-student-insert.php" method="post">
    Imię:<br><input type="text" name="firstname"><br>
    Nazwisko:<br><input type="text" name="lastname"><br>
    E-mail:<br><input type="text" name="email"><br>
    Indeks:<br><input type="number" name="studentIndex"><br>
    <input type="submit">
</form>
