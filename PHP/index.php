<h1 style="color:DarkBlue"> Witaj na stronie projektu LAMP </h1>

<h3 style="color:Blue"> Opis: </h3>
<h4> Strona powstała jako przykład zastosowania Dockera do wykorzystania LAMP. </h4>
Umożliwia ona odczytywanie i zapisywanie przykładowych danych studentów do bazy danych.

<h3 style="color:Blue"> Struktura projektu: </h3>
Projekt oparty jest o LAMP: <b>L</b>inux - <b>A</b>pache - <b>M</b>ySQL - <b>P</b>HP<br>
chociaż istnieją też inne narzędzia, które można podstawić pod ten sam akronim (Python, MariaDB...).<br>

<br>Celem projektu było utworzenie dokładnie trzech kontenerów, z których każdy zawierać będzie instancję jednego z tych narzędzi.


<h3 style="color:Blue"> Linki i źródła: </h3>
Więcej informacji o samym LAMP znaleźć można pod podanymi niżej linkami:

<ul>
  <li><a href="https://pl.wikipedia.org/wiki/LAMP">pl.wikipedia.org</a></li>
  <li><a href="https://hub.docker.com/r/mattrayner/lamp">Dcoker Hub</a></li>
  <li><a href="https://www.liquidweb.com/kb/what-is-a-lamp-stack/">Liquid Web</a></li>
</ul>

<br>Ale zasoby internetu są nieskończone i znaleźć można wiele więcej...<br>

<h2 style="color:DarkBlue"> Przykład użycia LAMP </h2>
Aby sprawdzić działanie LAMP z bazą danych MySQL <a href="mysql-example.php">kliknij tutaj</a>
<br>
