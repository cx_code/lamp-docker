<?php

$host = 'mysql';
$user = 'root';
$pass = 'password';
$database = 'StudentsDatabase';
$mysqli = new mysqli( $host, $user, $pass, $database );


if( $mysqli->connect_error )
{
    die( "Połączenie nie powiodło się: " . $mysqli->connect_error );
}
else
{
    $id = $_POST["id"];
    $table = 'Students';

    echo "<br> Usuwanie studenta z rekordu o numerze: ".$id."...<br>";
    $delete = "DELETE FROM ".$table." WHERE id = '$id'";

    if( $mysqli->query($delete) === TRUE )
    {
        echo "<br>Student usunięty z bazy danych!<br>";
    }
    else
    {
        echo "<br>Nie udało się usunąć studenta z bazy.<br>Przyczyną jest błąd: ".$mysqli->error ."<br>";
    }
    echo "Sprawdź swoją bazę wracając do ";
    echo "<a href=\"mysql-example.php\">przykładu LAMP</a><br>";
}
?>
