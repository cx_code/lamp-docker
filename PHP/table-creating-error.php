<?php

function CreateTableOnError()
{
    $host = 'mysql';
    $user = 'root';
    $pass = 'password';
    $database = 'StudentsDatabase';
    $mysqli = new mysqli( $host, $user, $pass, $database );

    echo "# Tabela nie istnieje - tworzenie nowej...<br>";
    $sql = "CREATE TABLE Students
    (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        email VARCHAR(50),
        studentIndex INT(6) UNSIGNED NOT NULL
    )";
    if( $mysqli->query($sql) === TRUE )
    {
        echo "# Tabela utworzona poprawnie!<br>";
    }
    else
    {
        echo "Błąd w tworzeniu tabeli: " . $mysqli->error;
    }
}

?>
